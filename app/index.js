//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3006;
const history = require("connect-history-api-fallback");
var path = require("path");

var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false,
  })
  .then(() => console.log("connection succesful"))
  .catch((err) => console.error(err));
mongoose.set("useCreateIndex", true);

var auth = require("./routes/auth");
var organization = require("./routes/organization");
var chapter = require("./routes/chapter");
var upload = require("./routes/upload");

app.use("/api/auth", auth);
app.use("/api/organization", organization);
app.use("/api/chapter", chapter);
app.use("/api/upload", upload);

var category = require("./routes/category");
app.use("/api/category", category);

var career = require("./routes/career");
app.use("/api/career", career);

var issue = require("./routes/issue");
app.use("/api/issue", issue);

var blog = require("./routes/blog");
app.use("/api/blog", blog);

var team = require("./routes/team");
app.use("/api/team", team);

var home = require("./routes/home");
app.use("/api/home", home);

var member = require("./routes/member");
app.use("/api/member", member);

app.use(history());
app.use(express.static(__dirname + "/../dist"));

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname + "/../dist/index.html"));
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
