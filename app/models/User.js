var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcryptjs");
var SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  group: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  university: {
    type: String
  },
  major: {
    type: String
  },
  graduating: {
    type: String
  },
  nationality: {
    type: String
  },
  gender: {
    type: String
  },
  wheredidyouhear: {
    type: String
  }
});

UserSchema.pre("save", function(next) {
  var user = this;
  console.log(this);
  if (this.isModified("password") || this.isNew) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) {
        return next(err);
      }
      console.log("salt", salt);
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        console.log(hash);
        user.password = hash;
        console.log(user.password);
        next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function(passw, cb) {
  console.log("compare", passw, this);
  bcrypt.compare(passw, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("User", UserSchema);
