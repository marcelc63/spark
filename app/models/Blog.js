var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var MySchema = new Schema({
  name: { type: String },
  content: { type: String },
  oneliner: { type: String },
  thumbnail: { type: String },
  date: { type: String },
  author: { type: String },
  authorthumbnail: { type: String },
  authorbio: { type: String }
});

MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Blog", MySchema);
