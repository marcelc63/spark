var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var SiteSchema = new Schema({
  author: { type: String },
  background: { type: String },
  header: { type: String },
  headerBackground: { type: String },
  headerWidth: { type: String },
  bodyWidth: { type: String },
  card: { type: String },
  cardCol: { type: String },
  title: { type: String },
  tagline: { type: String },
  searchPlaceholder: { type: String },
  backgroundColor: { type: String },
  backgroundImage: { type: String },
  headerColor: { type: String },
  headerImage: { type: String },
  headerRounded: { type: String },
  headerOverlay: { type: String },
  headerMarginTop: { type: String },
  bodyColor: { type: String },
  titleColor: { type: String },
  taglineColor: { type: String },
  cardColor: { type: String },
  cardRounded: { type: String },
  cardPrimaryColor: { type: String },
  cardSecondaryColor: { type: String },
  cardButtonColor: { type: String },
  cardButtonTextColor: { type: String },
  searchColor: { type: String },
  searchTextColor: { type: String },
  searchButtonColor: { type: String },
  searchButtonTextColor: { type: String },
  content: { type: String },
  collectionId: { type: String },
  owner: { type: ObjectId, ref: "User" }
});

SiteSchema.plugin(URLSlugs("title", { update: true }));

module.exports = mongoose.model("Site", SiteSchema);
