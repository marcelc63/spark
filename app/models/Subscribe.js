var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SubscribeSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports = mongoose.model("Subscribe", SubscribeSchema);
