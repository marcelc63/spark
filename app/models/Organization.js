var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var OrganizationSchema = new Schema({
  name: { type: String },
  content: { type: String },
  location: { type: String },
  website: { type: String },
  thumbnail: { type: String },
  thumbnail2: { type: String },
  logo: { type: String },
  category: [],
  career: [],
  issue: []
});

OrganizationSchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Organization", OrganizationSchema);
