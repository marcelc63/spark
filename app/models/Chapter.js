var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var ChapterSchema = new Schema({
  name: { type: String },
  location: { type: String },
  thumbnail: { type: String },
  upcoming: [
    {
      thumbnail: { type: String },
      content: { type: String }
    }
  ],
  gallery: [
    {
      thumbnail: { type: String }
    }
  ],
  past: [
    {
      title: { type: String },
      thumbnail: { type: String },
      content: { type: String }
    }
  ],
  testimony: [
    {
      content: { type: String },
      name: { type: String },
      title: { type: String }
    }
  ],
  team: [
    {
      name: { type: String },
      title: { type: String },
      bio: { type: String },
      thumbnail: { type: String }
    }
  ]
});

ChapterSchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Chapter", ChapterSchema);
