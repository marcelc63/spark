var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Subscribe = require("../models/Subscribe.js");
var passport = require("passport");
require("../config/passport")(passport);

//Dependencies
let multer = require("multer");
let storage = multer.memoryStorage();
let upload = multer({ storage: storage });
let axios = require("axios");
let credentials = require("../config/settings.js");
var cors = require("cors");

router.post("/image", upload.single("avatar"), cors(), function(req, res) {
  let image = req.file.buffer.toString("base64");
  axios({
    method: "post",
    url: "https://api.imgur.com/3/image",
    data: {
      image: image,
      album: "y6Y7amj",
      type: "base64"
    },
    headers: {
      Authorization: credentials.imgur.clientId,
      Authorization: credentials.imgur.brearer
    }
  })
    .then(packet => {
      console.log("Success");
      let url = packet.data.data.link;
      res.send({ link: url });
    })
    .catch(error => {});
});

router.post("/blog", upload.single("image"), cors(), function(req, res) {
  let image = req.file.buffer.toString("base64");
  axios({
    method: "post",
    url: "https://api.imgur.com/3/image",
    data: {
      image: image,
      album: "y6Y7amj",
      type: "base64"
    },
    headers: {
      Authorization: credentials.imgur.clientId,
      Authorization: credentials.imgur.brearer
    }
  })
    .then(packet => {
      console.log("Success");
      let url = packet.data.data.link;
      res.send(url);
    })
    .catch(error => {});
});

/* GET ALL */
router.get("/", function(req, res) {
  Subscribe.find(function(err, docs) {
    if (err) return next(err);
    res.json({ amount: docs.length });
  });
});

/* SAVE */
router.post("/", function(req, res) {
  Subscribe.create(req.body, function(err, post) {
    if (err) {
      return res.json({ success: false, msg: "Email already exists." });
    }
    // console.log("done");
    res.json({ success: true });
  });
});

module.exports = router;
