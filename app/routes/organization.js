var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Organization.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ALL */
router.get("/", function(req, res) {
  Model.find({}, function(err, sites) {
    if (err) return next(err);
    res.json(sites);
  });
});

/* GET ONE */
router.get("/:slug", function(req, res) {
  Model.findOne({ slug: req.params.slug }, function(err, sites) {
    if (err) return next(err);
    res.json(sites);
  });
});

/* SAVE */
router.post("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers);
  if (token) {
    let payload = {
      name: req.body.name,
      location: req.body.location,
      website: req.body.website,
      logo: req.body.logo,
      content: JSON.stringify(req.body.content),
      category: req.body.category,
      career: req.body.career,
      issue: req.body.issue,
      thumbnail: req.body.thumbnail,
      thumbnail2: req.body.thumbnail2
    };
    Model.create(payload, function(err, post) {
      if (err) return console.log(err);
      // console.log("done");
      res.json(post);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

/* UPDATE */
router.post(
  "/update",
  passport.authenticate("jwt", { session: false }),
  function(req, res, next) {
    var token = getToken(req.headers);
    if (token) {
      let payload = {
        name: req.body.name,
        location: req.body.location,
        website: req.body.website,
        logo: req.body.logo,
        content: JSON.stringify(req.body.content),
        category: req.body.category,
        career: req.body.career,
        issue: req.body.issue,
        thumbnail: req.body.thumbnail,
        thumbnail2: req.body.thumbnail2
      };
      Model.findOneAndUpdate({ _id: req.body._id }, payload, function(
        err,
        post
      ) {
        if (err) return next(err);
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

/* DELETE */
router.post(
  "/delete",
  passport.authenticate("jwt", { session: false }),
  function(req, res) {
    // console.log("delete");
    var token = getToken(req.headers);
    if (token) {
      Model.findByIdAndRemove(req.body._id, function(err, post) {
        if (err) return console.log(err);
        console.log("done");
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

module.exports = router;
