var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/Chapter.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ALL */
router.get("/", function(req, res) {
  Model.find({}, function(err, sites) {
    if (err) return next(err);
    res.json(sites);
  });
});

/* GET ONE */
router.get("/:slug", function(req, res) {
  Model.findOne({ slug: req.params.slug }, function(err, sites) {
    if (err) return next(err);
    res.json(sites);
  });
});

/* SAVE */
router.post("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers);
  if (token) {
    let payload = {
      name: req.body.name,
      location: req.body.location,
      thumbnail: req.body.thumbnail,
      upcoming: req.body.upcoming,
      gallery: req.body.gallery,
      past: req.body.past,
      testimony: req.body.testimony,
      team: req.body.team
    };
    Model.create(payload, function(err, post) {
      if (err) return console.log(err);
      // console.log("done");
      res.json(post);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

/* UPDATE */
router.post(
  "/update/",
  passport.authenticate("jwt", { session: false }),
  function(req, res, next) {
    let payload = {
      name: req.body.name,
      location: req.body.location,
      thumbnail: req.body.thumbnail,
      upcoming: req.body.upcoming,
      gallery: req.body.gallery,
      past: req.body.past,
      testimony: req.body.testimony,
      team: req.body.team
    };
    var token = getToken(req.headers);
    if (token) {
      Model.findOneAndUpdate({ _id: req.body._id }, payload, function(
        err,
        post
      ) {
        if (err) return next(err);
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

/* DELETE */
router.post(
  "/delete",
  passport.authenticate("jwt", { session: false }),
  function(req, res) {
    var token = getToken(req.headers);
    if (token) {
      Model.findByIdAndRemove(req.body._id, function(err, post) {
        if (err) return console.log(err);
        console.log("done");
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

module.exports = router;
