var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/User.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ALL */
router.get("/", function(req, res) {
  Model.find(function(err, docs) {
    console.log(docs);
    if (err) return next(err);
    res.json(docs);
  });
});

/* SAVE */
router.post("/", function(req, res) {
  Subscribe.create(req.body, function(err, post) {
    if (err) {
      return res.json({ success: false, msg: "Email already exists." });
    }
    // console.log("done");
    res.json({ success: true });
  });
});

module.exports = router;
