var mongoose = require("mongoose");
var passport = require("passport");
var settings = require("../config/settings");
require("../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var User = require("../models/User");

const mailjet = require("node-mailjet").connect(
  "8e933987c65e37ef45f56e7c583a4f78",
  "7abab28d53583af8f2ed3aa7528f0888"
);

function sendmail(email) {
  const request = mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "hello@sparksocialimpact.org",
          Name: "Spark Social Impact"
        },
        To: [
          {
            Email: email
          }
        ],
        TemplateID: 1029730,
        TemplateLanguage: true,
        Subject: "Welcome to the Spark Ecosystem!"
      }
    ]
  });
  request
    .then(result => {
      console.log(result.body);
    })
    .catch(err => {
      console.log(err.statusCode);
    });
}

router.post("/register", function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({ success: false, msg: "Please pass username and password." });
  } else {
    console.log("REGISTER", req.body);
    var newUser = new User({
      email: req.body.email,
      name: req.body.name,
      password: req.body.password,
      university: req.body.university,
      major: req.body.major,
      graduating: req.body.graduating,
      nationality: req.body.nationality,
      gender: req.body.gender,
      wheredidyouhear: req.body.wheredidyouhear,
      group: "Member"
    });
    // save the user
    newUser.save(function(err) {
      if (err) {
        console.log(err);
        return res.json({
          success: false,
          msg: "Username already exists."
        });
      }
      console.log("SUCCESS");
      //Email
      sendmail(req.body.email);
      res.json({
        success: true,
        msg: "Successful created new user."
      });
    });
  }
});

router.get("/mail", function(req, res) {
  sendmail();
  res.send("done");
});

router.post("/login", function(req, res) {
  User.findOne(
    {
      email: req.body.email
    },
    function(err, user) {
      if (err) throw err;

      if (!user) {
        res.status(401).send({
          success: false,
          msg: "Authentication failed. User not found."
        });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function(err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), settings.secret);
            // return the information including token as JSON

            res.json({
              success: true,
              token: "JWT " + token,
              group: user.group,
              name: user.name
            });
          } else {
            res.status(401).send({
              success: false,
              msg: "Authentication failed. Wrong password."
            });
          }
        });
      }
    }
  );
});

router.post("/logout", function(req, res) {
  req.logout();
  res.json({ success: true });
});

module.exports = router;
