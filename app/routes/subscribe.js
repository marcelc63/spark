var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Subscribe = require("../models/Subscribe.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ALL */
router.get("/", function(req, res) {
  Subscribe.find(function(err, docs) {
    if (err) return next(err);
    res.json({ amount: docs.length });
  });
});

/* SAVE */
router.post("/", function(req, res) {
  Subscribe.create(req.body, function(err, post) {
    if (err) {
      return res.json({ success: false, msg: "Email already exists." });
    }
    // console.log("done");
    res.json({ success: true });
  });
});

module.exports = router;
