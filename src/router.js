import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/about",
      name: "About",
      component: () => import("./views/About.vue")
    },
    {
      path: "/approach",
      name: "Approach",
      component: () => import("./views/Approach.vue")
    },
    {
      path: "/contact",
      name: "Contact",
      component: () => import("./views/Contact.vue")
    },
    {
      path: "/involvechapter",
      name: "InvolveChapter",
      component: () => import("./views/InvolveChapter.vue")
    },
    {
      path: "/involvemember",
      name: "InvolveMember",
      component: () => import("./views/InvolveMember.vue")
    },
    {
      path: "/blog",
      name: "Blog",
      component: () => import("./views/Blog.vue")
    },
    {
      path: "/post/:slug",
      name: "Post",
      component: () => import("./views/Post.vue")
    },
    {
      path: "/team",
      name: "Team",
      component: () => import("./views/Team.vue")
    },
    {
      path: "/chapter/:slug",
      name: "Chapter",
      component: () => import("./views/Chapter.vue")
    },
    {
      path: "/start",
      name: "Start",
      component: () => import("./views/Start.vue")
    },
    {
      path: "/directory",
      name: "Directory",
      component: () => import("./views/Directory.vue")
    },
    {
      path: "/directorysingle",
      name: "DirectorySingle",
      component: () => import("./views/DirectorySingle.vue")
    },
    {
      path: "/register",
      name: "Register",
      component: () => import("./views/Register.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/registersuccess",
      name: "RegisterSuccess",
      component: () => import("./views/RegisterSuccess.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/login",
      name: "Login",
      component: () => import("./views/Login.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/admin",
      name: "Admin",
      component: () => import("./views/Admin/Home.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: "/admin/sid",
      name: "Admin",
      component: () => import("./views/Admin/SID.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: "/admin/blog",
      name: "Admin",
      component: () => import("./views/Admin/Blog.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: "/admin/team",
      name: "Admin",
      component: () => import("./views/Admin/Team.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: "/admin/chapter",
      name: "Admin",
      component: () => import("./views/Admin/Chapter.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
    {
      path: "/admin/member",
      name: "Admin",
      component: () => import("./views/Admin/Member.vue"),
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("jwtToken") == null) {
      next({
        path: "/",
        params: { nextUrl: to.fullPath }
      });
    } else {
      next();
      let group = localStorage.getItem("group");
      if (to.matched.some(record => record.meta.is_admin)) {
        if (group == "Admin") {
          next();
        } else {
          next({ name: "Home" });
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem("jwtToken") == null) {
      next();
    } else {
      next({ name: "Home" });
    }
  } else {
    next();
  }
});

export default router;
