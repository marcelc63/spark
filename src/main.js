import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./main.css";

Vue.config.productionTip = false;

import vSelect from "vue-select";
Vue.component("v-select", vSelect);
import "vue-select/dist/vue-select.css";

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {
  image: {
    uploadURL: process.env.VUE_APP_API + "/api/upload/blog",
    dropzoneOptions: {
      paramName: "image"
    }
  }
});
import "vue-wysiwyg/dist/vueWysiwyg.css";

import VueCarousel from "vue-carousel";
Vue.use(VueCarousel);

new Vue({
  router,
  store,
  created: function() {
    if (localStorage.getItem("jwtToken") !== null) {
      this.$store.dispatch("change", { key: "loggedIn", val: true });
    }
  },
  render: h => h(App)
}).$mount("#app");
