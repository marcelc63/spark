import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false
  },
  mutations: {
    change: function(state, payload) {
      let { key, val } = payload;
      state[key] = val;
    }
  },
  actions: {
    change: function({ commit }, payload) {
      commit("change", payload);
    }
  },
  getters: {
    state: function(state) {
      return state;
    }
  }
});
